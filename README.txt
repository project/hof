
hof.module		Scott Courtney (Drupal ID "syscrusher")
				scott@4th.com  http://4th.com/

License:	GNU General Public License (http://www.gnu.org/)

RELEASE STATE:	This is a BETA RELEASE, not for production use
		This version is for Drupal 5.x only.

HOF, or "Hall of Fame", is a module that provides publicly-visible
summary statistics for a Drupal web site. Rather than providing a lot
of data on a node-by-node basis, which would likely be interesting only
to administrators, this module provides overall site statistics like
how many articles have been published, how long the site has been
online, how many active users are registered, and so on.

In addition to these aggregate statistics, the module can show a list
of the most active contributors, the most popular stories, and other
interesting credits for the past week, the past month, the past year,
and since the site's creation.

This module is extremely configurable by the administrator, and has
extensive explanations and help text in the admin screens.

The module provides the following paths:

	hof		Main Hall of Fame page, by default shows a
			table of contents of the other pages.

	hof/toc		Shows the table of contents regardless of
			administrative settings for the default page.

	hof/all		Shows all available (and permitted) statistics
			in a single page

	hof/site	Shows summary statistics for the whole site

	hof/contrib	Shows the most active contributors

	hof/node	Shows the most popular nodes

	hof/comment	Shows the most active commentors

	hof/bytype	Shows content by node type

	hof/files	Shows uploaded file counts

View the INSTALL.txt file and the online help within the HOF module for
additional details.
